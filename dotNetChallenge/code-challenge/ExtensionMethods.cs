﻿using System.Linq;

namespace CodeChallenge
{
    public static class StringExtensions
    {
        public static string ReverseString(this string input)
        {
            char[] charArray = input.ToCharArray().Reverse().ToArray();
            return new string(charArray);
        }
    }
}
