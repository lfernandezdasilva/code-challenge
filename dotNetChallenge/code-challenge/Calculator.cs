﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Char;


namespace CodeChallenge
{
    public static class Calculator
    {
        //We need to use string, since the given number can have an indefinite length, and the ulong type can be up to only 20 digits long

        public static string SumSetsOfValues(string firstInput, string secondInput)
        {
            //I validate the input again at the function, in case it is used in multiple places.
            ValidateCalculatorInput(firstInput);
            ValidateCalculatorInput(secondInput);

            //As requested in the exercise, the second input is reversed. An extension method was created for this purpose (ExthensionMethod.cs)
            secondInput = secondInput.ReverseString();

            string largerInput;
            string shorterInput;

            //Find which string is larger
            if (firstInput.Length > secondInput.Length)
            {
                largerInput = firstInput;
                shorterInput = secondInput;
            }
            else
            {
                largerInput = secondInput;
                shorterInput = firstInput;
            }

            //Find the difference between the length of the largerInput and shorterInput, to properly do the sum later
            var difference = largerInput.Length - shorterInput.Length;

            int[] firstNumbers = shorterInput.ToCharArray().Select(charNumber => (int)Char.GetNumericValue(charNumber)).ToArray();

            int[] secondNumbers = largerInput.ToCharArray().Select(charNumber => (int)Char.GetNumericValue(charNumber)).ToArray();

            var result = "";
            var extraDigit = 0;

            for (int i = shorterInput.Length - 1; i >= 0; i--)
            {
                var sum = firstNumbers[i] + secondNumbers[i + difference] + extraDigit;
                var remainder = sum % 10;

                result += remainder.ToString();

                extraDigit = sum / 10;
            }

            for (int i =difference - 1; i >= 0; i--)
            {
                int sum = secondNumbers[i]  + extraDigit;

                var remainder = sum % 10;

                result += remainder.ToString();

                extraDigit = sum / 10;
            }

            //Check if there is any extra digit left
            if (extraDigit > 0)
                result += extraDigit;

            //We need to reverse the string to display the result correctly
            result = result.ReverseString();
            
            return result;

        }

        public static void ValidateCalculatorInput(string input)
        {
            if (input != null && !input.All(IsDigit)) //I assume that only positive numbers will be used for the exercise
                throw new ArgumentException($"Input {input} contains a non-numeric character.");
        }

    }
}
