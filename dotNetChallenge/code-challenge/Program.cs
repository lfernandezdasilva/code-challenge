﻿using System;
using System.Linq;
using CodeChallenge;
using static System.Char;

namespace CodeChallenge.Main
{
    internal class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine("Please insert first input");

           string firstInput = Console.ReadLine();

           try
           {
               Calculator.ValidateCalculatorInput(firstInput);
           }
           catch (Exception ex)
           {
               Console.WriteLine(ex.Message);
               return;
           }

           Console.WriteLine("Please insert second input");

           string secondInput = Console.ReadLine();

            try
            {
                Calculator.ValidateCalculatorInput(secondInput);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            
            string result = Calculator.SumSetsOfValues(firstInput, secondInput);

            Console.WriteLine("The result is "+result);
        }
    }
}
