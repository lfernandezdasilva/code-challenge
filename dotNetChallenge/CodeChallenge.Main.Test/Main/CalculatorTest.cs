using System;
using FluentAssertions;
using Xunit;

namespace CodeChallenge.Main.Test
{
    public class CalculatorTest
    {
        [Theory]
        [InlineData("123456","123456", "777777")]
        [InlineData("150","46", "214")]
        [InlineData("4231","8546", "10689")]
        [InlineData("1","2", "3")]
        [InlineData("12345678912345", "12345678912345", "66667666566666")]
        public void SumSetsOfValues_WithValidInput_ShouldSumValues(string firstInput, string secondInput, string expectedResult)
        {
            var result = Calculator.SumSetsOfValues(firstInput, secondInput);

            result.Should().Be(expectedResult);

        }

        [Theory]
        [InlineData("123456", "A123456")]
        [InlineData("150A", "46")]
        [InlineData("150%A", "4$6")]
        [InlineData("1503$/4", "4($%6")]
        public void SumSetsOfValues_WithInvalidInput_ThrowsArgumentException(string firstInput, string secondInput)
        {
            Action action = () => Calculator.SumSetsOfValues(firstInput, secondInput);

            action.Should().Throw<ArgumentException>();

        }
    }
}
